#!/bin/bash

rm -rf ij-raw
rm -rf ij-decoded
rm -rf ij-repacked
rm recoded.apk injected.apk aligned.apk man.xml*
mkdir ij-raw
unzip -d ij-raw $1

apktool d $1 ij-decoded
cp ij-decoded/AndroidManifest.xml man.xml.orig
node patchmanifest.js
cp man.xml ij-decoded/AndroidManifest.xml

apktool b ij-decoded recoded.apk
mkdir ij-repacked
unzip -d ij-repacked recoded.apk
cp ij-repacked/AndroidManifest.xml ij-raw/AndroidManifest.xml
rm -rf ij-raw/META-INF

pushd ij-raw
zip -8 -n .dll:.wav -r ../injected.apk *
popd




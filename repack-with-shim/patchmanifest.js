var fs= require('fs');
var x = require('xmldom');
var d = fs.readFileSync('man.xml.orig');

var doc = new x.DOMParser().parseFromString(d.toString('utf8'));

var providers = doc.getElementsByTagName('provider');
var last_provider;
for(var i = 0; i < providers.length; ++i) {
  var provider = providers.item(i);
  if(provider.getAttribute("android:process") == ":bdservice_v1") {
    provider.parentNode.removeChild(provider);
    continue;
  }
  if(provider.getAttribute("android:initOrder") == "2147483647") {
    provider.setAttribute("android:initOrder", "2147483646");
    last_provider = provider;
    continue;
  }
}
var shim = doc.createElement("provider");
shim.setAttribute("android:name","mono.Shim");
shim.setAttribute("android:exported","false");
shim.setAttribute("android:initOrder","2147483647");
shim.setAttribute("android:authorities", "mono.Shim.__lib_init__")

last_provider.parentNode.appendChild(shim);

var s = new x.XMLSerializer().serializeToString(doc);
fs.writeFileSync('man.xml', s);
